import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from './routes';
import store from './store';
import App from './components/App';

Vue.use(VueRouter);

const router = new VueRouter({ routes });

/* eslint-disable-next-line no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
});
