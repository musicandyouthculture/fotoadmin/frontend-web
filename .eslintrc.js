module.exports = {
  root: true,

  env: {
    browser: true,
  },

  extends: [
    'plugin:vue/recommended',
    'semistandard',
  ],

  plugins: [
    'vue',
  ],

  rules: {
    'comma-dangle': ['error', 'always-multiline'],
  },
};
